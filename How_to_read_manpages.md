# The Question

While researching another problem, I [came across a command](https://superuser.com/questions/408060/how-to-use-locate-to-search-for-folders-only#408073),

    locate something | xargs -I {} bash -c "if [ -d "{}" ]; then echo {}; fi"

that I wanted to learn more about.  So I ran `man xargs` and get the following output:

    XARGS(1)                    General Commands Manual                   XARGS(1)

    NAME
           xargs - build and execute command lines from standard input

    SYNOPSIS
           xargs  [-0prtx]  [-E  eof-str] [-e[eof-str]] [--eof[=eof-str]] [--null]
           [-d delimiter] [--delimiter delimiter]  [-I  replace-str]  [-i[replace-
           str]]    [--replace[=replace-str]]   [-l[max-lines]]   [-L   max-lines]
           [--max-lines[=max-lines]] [-n max-args] [--max-args=max-args] [-s  max-
           chars]  [--max-chars=max-chars]  [-P max-procs] [--max-procs=max-procs]
           [--interactive]      [--verbose]      [--exit]      [--no-run-if-empty]
           [--arg-file=file]   [--show-limits]   [--version]   [--help]   [command
           [initial-arguments]]

    DESCRIPTION
           This manual page documents the GNU version of xargs...

I am trying to get better at using documentation to learn about Linux programs, but that "Synopsis" section is intimidating to new users.  It literally looks like gibberish compared to `man locate` or `man free`.

So far, I understand that square brackets mean optional and nested brackets mean options in optional.  But how am I supposed to induce a valid command with that?

<strong>I am not asking for help with xargs here.</strong>  I am looking for help interpreting a man page to understand complicated commands.  I want to stop making Google-indexed web blogs and personal help from others my first approach to learning Linux commands.



# The Answer

Well, this is my very personal way to read manpages:

# The manpager

When you open a manpage using the `man` command, the output will be displayed/rendered by the `less` or `more` commands, or any other command that will be set as your pager(manpager).

If you are using Linux you are probably served with your man infrastructure already configured to use `/usr/bin/less -is` (unless you installed some minimal distro) as [`man(1)`][1], explain on it's Options section:

    -P pager
    Specify which pager to use. This option overrides the MANPAGER environment variable, 
    which in turn overrides the PAGER variable. By default, man uses /usr/bin/less -is.

On FreeBSD and OpenBSD is just a matter of editing the `MANPAGER` environment variable since they will mostly use `more`, and some features like search and text highlight could be missing.

There is a good answer to the question of what differences `more`, `less` and `most` have [here][2](never used `most`). The ability to scroll backwards and scroll forward by page with <kbd>Space</kbd> or both ways by line with <kbd>↓</kbd> or <kbd>↑</kbd>(also, using `vi` bindings <kbd>j</kbd> and <kbd>k</kbd>) is essential while browsing manpages. Press <kbd>h</kbd> while using `less` to see the summary of commands available.

And that's why I suggest you to use `less` as your man pager. `less` have some essential features that will be used during this answer.

# How is a command formatted?

[Utility Conventions][3]: The Open Group Base Specifications Issue 7 - IEEE Std 1003.1, 2013 Edition. You should visit that link before trying to understand a manpage. This online reference describes the argument syntax of the standard utilities and introduces terminology used throughout POSIX.1-2017 for describing the arguments processed by the utilities. This will also indirectly get you updated about the real meaning of words like parameters, arguments, argument option...

The head of any manpage will look less cryptic to you after understanding the notation of the utility conventions:

    utility_name[-a][-b][-c option_argument]
        [-d|-e][-f[option_argument]][operand...]

# Have in mind what you want to do.

When doing your research about `xargs` you did it for a purpouse, right? You had a specific need that was reading standard output and executing commands based on that output.

# But, when I don't know which command I want?

Use `man -k` or `apropos` (they are equivalent). If I don't know how to find a file: `man -k file | grep search`. Read the descriptions and find one that will better fit your needs. Example:

    apropos -r '^report'
    bashbug (1)          - report a bug in bash
    df (1)               - report file system disk space usage
    e2freefrag (8)       - report free space fragmentation information
    filefrag (8)         - report on file fragmentation
    iwgetid (8)          - Report ESSID, NWID or AP/Cell Address of wireless network
    kbd_mode (1)         - report or set the keyboard mode
    lastlog (8)          - reports the most recent login of all users or of a given user
    pmap (1)             - report memory map of a process
    ps (1)               - report a snapshot of the current processes.
    pwdx (1)             - report current working directory of a process
    uniq (1)             - report or omit repeated lines
    vmstat (8)           - Report virtual memory statistics

Apropos works with regular expressions by default, (`man apropos`, read the description and find out what `-r` does), and on this example I'm looking for every manpage where the description starts with "report". 

To look for information related with reading standard input/output processing and reaching `xargs` as a possible option:

    man -k command| grep input
    xargs (1)            - build and execute command lines from standard input


# Always read the `DESCRIPTION` before starting

Take a time and read the description. By just reading the description of the `xargs` command we will learn that:

 - `xargs` reads from STDIN and executes the command needed. This also means that you will need to have some knowledge of how standard input works, and how to manipulate it through pipes to chain commands
 - The default behavior is to act like `/bin/echo`. This gives you a little tip that if you need to chain more than one `xargs`, you don't need to use echo to print.
 - We have also learned that unix filenames can contain blank and newlines, that this could be a problem and the argument `-0` is a way to prevent things explode by using null character separators. The description  warns you that the command being used as input needs to support this feature too, and that GNU `find` support it. Great. We use a lot of find with `xargs`.
 - `xargs` will stop if exit status 255 is reached.

Some descriptions are very short and that is generally because the software works on a very simple way. **Don't even think of skipping this part of the manpage** ;)

# Other things to pay attention...

You know that you can search for files using `find`. There is a ton of options and if you only look at the `SYNOPSIS`, you will get overwhelmed by those. It's just the tip of the iceberg. Excluding `NAME`, `SYNOPSIS`, and `DESCRIPTION`, you will have the following sections:

 - `AUTHORS`: the people who created or assisted in the creation of the
   command.
   
 - `BUGS`: lists any known defects. Could be only implementation limitations.
   
 - `ENVIRONMENT`: Aspects of your shell that could be affected by the command, or variables that will be used.
   
 - `EXAMPLES` or `NOTES`: Self explanatory.

 - `REPORTING BUGS`: Who you will have to contact if you find bugs on this tool or in its documentation.
   
 - `COPYRIGHT`: Person who created and disclaimers about the software. All related with the license of the software itself.
   
 - `SEE ALSO`: Other commands, tools or working aspects that are related to this command, and could not fit on any of the other sections.

You will most probably find interesting info about the aspects you want of a tool on the examples/notes section.

# Example

On the following steps I'll take `find` as an example, since it's concepts are "more simple" than `xargs` to explain(one command find files and the other deals with stdin and pipelined execution of other command output). Let's just pretend that we [know nothing][4](or very little) about this command. 

I have a specific problem that is: I have to look for every file with the `.jpg` extension, and with 500KiB (KiB = 1024 byte, commonly called kibibyte), or more in size inside a ftp server folder.

First, open the manual: `man find`. The `SYNOPSIS` is slim. Let's search for things inside the manual:  Type <kbd>/</kbd> plus the word you want (`size`). It will index a lot of entries `-size` that will count specific sizes. Got stuck. Don't know how to search with "more than" or "less than" a given size, and the man does not show that to me.

Let's give it a try, and search for the next entry found by hitting <kbd>n</kbd>. OK. Found something interesting: `find` `\( -size +100M -fprintf /root/big.txt %-10s %p\n \)`. Maybe this example is showing us that with `-size +100M` it will find files with 100MB or more. How could I confirm? Going to the head of the manpage and searching for other words.

Again, let's try the word `greater`. Pressing <kbd>g</kbd> will lead us to the head of the manpage. <kbd>/</kbd>`greater`, and the first entry is:

     Numeric arguments can be specified as
    
        +n     for **greater** than n,
    
        -n     for less than n,
    
         n      for exactly n.

Sounds great. It seems that this block of the manual confirmed what we suspected. However, this will not only apply to file sizes. It will apply to any `n` that can be found on this manpage (as the phrase said: "Numeric arguments can be specified as").

Good. Let us find a way to filter by name: <kbd>g</kbd> <kbd>/</kbd>`insensitive`. Why? Insensitive? Wtf? We have a hypothetical ftp server, where "that other OS" people could give a file name with extensions as `.jpg`, `.JPG` , `.JpG`. This will lead us to:

    -ilname pattern
                  Like  -lname,  but  the  match  is  case insensitive.  If the -L
                  option or the -follow option is in  effect,  this  test  returns
                  false unless the symbolic link is broken.

However, after you search for `lname` you will see that this will only search for symbolic links. We want real files. The next entry: 

       -iname pattern
              Like -name, but the match is case insensitive.  For example, the
              patterns `fo*' and `F??' match  the  file  names  `Foo',  `FOO',
              `foo',  `fOo',  etc.   In these patterns, unlike filename expan‐
              sion by the shell, an initial '.' can be matched by  `*'.   That
              is, find -name *bar will match the file `.foobar'.   Please note
              that you should quote patterns as a matter of course,  otherwise
              the shell will expand any wildcard characters in them.

Great. I don't even need to read about `-name` to see that `-iname` is the case insensitive version of this argument. Lets assemble the command:

**Command:**  `find /ftp/dir/ -size +500k -iname "*.jpg"`

What is implicit here: The knowledge that the wildcard `?` represents "any character at a single position" and `*` represents "zero or more of any character". The `-name` parameter will give you a summary of this knowledge.

# Tips that apply to all commands

Some options, mnemonics and "syntax style" travel through all commands making you buy some time by not having to open the manpage at all. Those are learned by practice and the most common are:

 - Generally, `-v` means verbose. `-vvv` is a variation "very very verbose" on some software.
 - Following the POSIX standard, generally one dash arguments can be stacked. Example: `tar -xzvf`,  `cp -Rv`.
 - Generally `-R` and/or `-r` means recursive.
 - Almost all commands have a brief help with the `--help` option.
 - `--version` shows the version of a software.
 - `-p`, on copy or move utilities means "preserve permissions".
 - `-y` means YES, or "proceed without confirmation" in most cases.

Note that the above are not always true though. For example, the `-r` switch can mean [very different things](https://unix.stackexchange.com/q/92595/34720) for different software. It is always a good idea to check and make sure when a command could be dangerous, but these are common defaults. 

# Default values of commands.

At the pager chunk of this answer, we saw that `less -is` is the pager of `man`. The default behavior of commands are not always shown at a separated section on manpages, or at the section that is most top placed.

You will have to read the options to find out defaults, or if you are lucky, typing <kbd>/</kbd>`pager` will lead you to that info. This also requires you to know the concept of the pager(software that scrolls the manpage), and this is a thing you will only acquire after reading lots of manpages.

Why is that important? This will open up your perception if you find differences on scroll and color behavior while reading [`man(1)`][1] on Linux(`less -is` pager) or FreeBSD [`man(1)`][5] for example.

# And what about the `SYNOPSIS` syntax?

After getting all the information needed to execute the command, you can combine options, option-arguments and operands inline to make your job done. Overview of concepts:

 - Options are the switches that dictates a command behavior. "*Do this*"
   "*don't do this*" or "*act this way*". Often called switches. 
 - Option-arguments are used on most cases when an option isn´t
   binary(on/off) like `-t` on mount, that specifies the type of a
   filesystem(`-t iso9660`, `-t ext2`). "*Do this with closed eyes*" or
   "*feed the animals, but only the lions*". Also called arguments.
 - Operands are things you want that command to act upon. If you use
   `cat file.txt`, the operand is a file inside your current
   directory, and it´s contents will be shown on `STDOUT`. `ls` is a
   command where an operand is optional. The three dots after the operand
   implicitly tells you that `cat` can act on multiple operands(files) at 
   the same time. You may notice that some commands have set what type of
   operand it will use. Example: `cat [OPTION] [FILE]...`  

**Related synopsis stuff:**

 - [Understand synopsis in manpage][6]

# When will this method not work?

 - Manpages that have no examples
 - Manpages where options have a short explanation
 - When you use generic keywords like `and`, `to`, `for` inside the manpages
 - Manpages that are not installed. It seems to be obvious but, if you don't have `lftp` (and its manpages) installed you can't know that is a suitable option as a more sophisticated ftp client by running `man -k ftp`

In some cases the examples will be pretty simple, and you will have to make some executions of your command to test, or in a worst case scenario, Google it.
 
# Other: Programming languages and it's modules:

If you are programming or just creating scripts, keep in mind that some languages have it's own manpages systems, like `perl`(`perldocs`), python(`pydocs`), etc, holding specific information about methods/funcions, variables, behavior, and other important information about the module you are trying to use and learn. This was useful to me when i was creating a script to download unread IMAP emails using the [`perl Mail::IMAPClient`][7] module.

You will have to figure out those specific manpages by using `man -k` or searching online. Examples:

    [root@host ~]# man -k doc | grep perl
    perldoc              (1)  - Look up Perl documentation in Pod format


    [root@host ~]# perldoc Mail::IMAPClient
    IMAPCLIENT(1)         User Contributed Perl Documentation        IMAPCLIENT(1)
    
    NAME
           Mail::IMAPClient - An IMAP Client API
    
    SYNOPSIS
             use Mail::IMAPClient;
    
             my $imap = Mail::IMAPClient->new(
               Server   => ’localhost’,
               User     => ’username’,
               Password => ’password’,
               Ssl      => 1,
               Uid      => 1,
             );
   ...tons of other stuff here, with sections like a regular manpage...

With python:

    [root@host ~]# pydoc sys
    Help on built-in module sys:
    
    NAME
        sys
    
    FILE
        (built-in)
    
    MODULE DOCS
        http://www.python.org/doc/current/lib/module-sys.html
    
    DESCRIPTION
        This module provides access to some objects used or maintained by the
        interpreter and to functions that interact strongly with the interpreter.
    ...again, another full-featured manpage with interesting info...

Or, the `help()` funcion inside python shell if you want to read more details of some object:

    nwildner@host:~$ python3.6
    Python 3.6.7 (default, Oct 21 2018, 08:08:16)
    [GCC 8.2.0] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> help(round)
    
    Help on built-in function round in module builtins:
    
    round(...)
        round(number[, ndigits]) -> number
    
        Round a number to a given precision in decimal digits (default 0 digits).
        This returns an int when called with one argument, otherwise the
        same type as the number. ndigits may be negative.


**Bonus:** The `wtf` command can help you with acronyms and it works as `whatis` if no acronym on it's database is found, but what you are searching is part of the man database. On Debian this command is part of the [`bsdgames`][8] package. Examples: 

    nwildner@host:~$ wtf rtfm
    RTFM: read the fine/fucking manual
    nwildner@host:~$ wtf afaik
    AFAIK: as far as I know
    nwildner@host:~$ wtf afak
    Gee...  I don't know what afak means...
    nwildner@host:~$ wtf tcp
    tcp: tcp (7)              - TCP protocol.
    nwildner@host:~$ wtf systemd
    systemd: systemd (1)          - systemd system and service manager



  [1]: https://linux.die.net/man/1/man
  [2]: https://unix.stackexchange.com/q/81129/34720
  [3]: http://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap12.html
  [4]: http://en.wikipedia.org/wiki/Jon_Snow_(character)
  [5]: https://www.freebsd.org/cgi/man.cgi?query=man&apropos=0&sektion=1&manpath=FreeBSD%2011.1-RELEASE%20and%20Ports&arch=default&format=html
  [6]: https://unix.stackexchange.com/q/17833/34720
  [7]: http://search.cpan.org/~plobbes/Mail-IMAPClient-3.38/lib/Mail/IMAPClient.pod
  [8]: https://packages.debian.org/jessie/bsdgames
