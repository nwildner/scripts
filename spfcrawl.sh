#!/bin/bash
# spfcrawl by Nwildner

spfcrawl() {
dig -t TXT +short $1 | tr ' ' '\n' | egrep '(^include|^ip4)' | while read i; do
	case "$i" in
		ip4:*)  echo ${i#*:} ;; 
		include:*)  spfcrawl ${i#*:} ;; 
	esac
done;
}

spfcrawl $1 
